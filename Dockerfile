FROM java:8-jdk-alpine
COPY  $WORKSPACE/cert/mavfin.p12 /usr/share
COPY  $WORKSPACE/target/*.jar /home/ubuntu/app/
WORKDIR /home/ubuntu/app/
ENTRYPOINT ["java", "-jar", "mavfin-userservice-0.0.1-SNAPSHOT.jar"]
