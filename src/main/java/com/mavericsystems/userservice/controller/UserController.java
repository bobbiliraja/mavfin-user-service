package com.mavericsystems.userservice.controller;

import com.mavericsystems.userservice.domain.AppUser;
import com.mavericsystems.userservice.util.DistributedConfigProperties;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private DistributedConfigProperties distributedConfigProperties;

    @Autowired
    private RestTemplate restTemplate;

    private static final String USER_ID="userId";

    private static final String TENANT_IDENTIFIER="tenantIdentifier";

    private static final String TENANTIDENT_DEFAULT="default";


    @CrossOrigin
    @PostMapping("/createUser")
    public String createUser(@RequestBody AppUser appUser)
    {
        String result;
        try
        {
            HttpHeaders httpHeaders=getHeaders();

            HttpEntity<AppUser> requestEntity=new HttpEntity<>(appUser,httpHeaders);
            ResponseEntity<String> response =
                    restTemplate.exchange(distributedConfigProperties.getUserURL(),
                            HttpMethod.POST, requestEntity, String.class);
            result= response.getBody();
        }
        catch(Exception e)
        {
            result="Exception occured while creating User";
        }

        return result;
    }

    @CrossOrigin
    @PutMapping("/userUpdate/{userId}")
    public String userUpdate(@PathVariable("userId") Long id, @RequestBody String request)
    {
        String result;
        try
        {
            HttpHeaders httpHeaders = getHeaders();
            httpHeaders.setContentType(org.springframework.http.MediaType.APPLICATION_JSON);
            Map<String,Long> params =new HashMap<>();
            params.put(USER_ID,id);

            HttpEntity<String> requestEntity=new HttpEntity<>(request,httpHeaders);
            UriComponentsBuilder builder =UriComponentsBuilder.
                    fromUriString(distributedConfigProperties.getUpdateURL()).queryParam(TENANT_IDENTIFIER,TENANTIDENT_DEFAULT);
            URI uri=builder.buildAndExpand(params).toUri();
            ResponseEntity<String> response=restTemplate.exchange(uri,HttpMethod.PUT,requestEntity, String.class);

             result=response.getBody();

        }
        catch (Exception e)
        {
             result="Exception occured while updating User";
        }

            return result;
    }

    @CrossOrigin
    @PostMapping("/userAuth")
    public String userAuth(@Param("username") String username, @Param("password") String password)
    {
        String result;
        try
        {
            HttpHeaders httpHeaders = getHeaders();
            httpHeaders.setContentType(org.springframework.http.MediaType.APPLICATION_JSON);


            HttpEntity<String> requestEntity=new HttpEntity<>(httpHeaders);
            Map<String,String> params =new HashMap<>();
            params.put("username",username);
            params.put("password",password);

            UriComponentsBuilder builder =UriComponentsBuilder.
                    fromUriString(distributedConfigProperties.getAuthUrl())
                    .queryParam("username",username)
                    .queryParam("password",password)
                    .queryParam(TENANT_IDENTIFIER,TENANTIDENT_DEFAULT);
            URI uri=builder.buildAndExpand(params).toUri();
            ResponseEntity<String> response=restTemplate.exchange(uri,HttpMethod.POST,requestEntity, String.class);

            result=response.getBody();

        }
        catch (Exception e)
        {
            result="Exception occured while authenticating User";
        }

        return result;
    }

    private HttpHeaders getHeaders(){

        HttpHeaders headers=new HttpHeaders();
        StringBuilder auth = new StringBuilder("");
        auth.append(distributedConfigProperties.getUserName()).append(":")
                .append(distributedConfigProperties.getPassword());
        byte[] encodedAuth = Base64.encodeBase64(
                auth.toString().getBytes(Charset.forName(String.valueOf(StandardCharsets.US_ASCII))) );
        String authHeader = "Basic " + new String( encodedAuth );
        headers.set( "Authorization", authHeader );
        return headers;
    }

}
