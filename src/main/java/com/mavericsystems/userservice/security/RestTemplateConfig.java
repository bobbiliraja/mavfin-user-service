package com.mavericsystems.userservice.security;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Configuration
public class RestTemplateConfig {

    Logger logger = LoggerFactory.getLogger(RestTemplateConfig.class);

    @Value("${server.ssl.key-store-type}")
    private String keystoreType;

    @Value("${server.ssl.key-store}")
    private String keystorePath;

    @Value("${server.ssl.key-store-password}")
    private String keystorePwd;

    @Bean
    public RestTemplate getRestTemplate()
            throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, IOException {
        RestTemplate restTemplate = new RestTemplate();
        KeyStore keyStore = null;
        InputStream in = new FileInputStream(keystorePath);
        HttpComponentsClientHttpRequestFactory factory = null;
        try {
            keyStore = KeyStore.getInstance(keystoreType);
            keyStore.load(in, keystorePwd.toCharArray());
            SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory
                    (new SSLContextBuilder().
                            loadTrustMaterial(null, new TrustSelfSignedStrategy())
                            .loadKeyMaterial(keyStore, keystorePwd.toCharArray()).build(),
                            NoopHostnameVerifier.INSTANCE);
            HttpClient httpClient = HttpClients.custom().
                    setSSLSocketFactory(socketFactory)
                    .setMaxConnTotal(5)
                    .setMaxConnPerRoute(5)
                    .build();

            factory = new HttpComponentsClientHttpRequestFactory(httpClient);
            factory.setReadTimeout(10000);
            factory.setConnectTimeout(10000);
            restTemplate.setRequestFactory(factory);
        } catch (IOException | CertificateException | UnrecoverableKeyException e) {
            logger.error("Error Occured", e);
        }
        finally {
            in.close();
        }
        return restTemplate;
    }

}
