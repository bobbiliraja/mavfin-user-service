package com.mavericsystems.userservice.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AppUser implements Serializable {

    private String email;

    private String firstname;

    private String lastname;

    private int officeId;

    private String password;

    private String repeatPassword;

    private List<Integer> roles;

    private String sendPasswordToEmail;

    private int staffId;

    private String username;

}

