package com.mavericsystems.userservice.controller;


import com.mavericsystems.userservice.domain.AppUser;
import com.mavericsystems.userservice.util.DistributedConfigProperties;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.anyString;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerTest {

    @InjectMocks
    UserController userController;

    @Mock
    private DistributedConfigProperties distributedConfigProperties;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private RequestEntity requestEntity;

    @Mock
    private HttpHeaders httpHeaders;

    @Before
    public void setUp()
    {
        Mockito.when(distributedConfigProperties.getUserName()).thenReturn("mifos");
        Mockito.when(distributedConfigProperties.getPassword()).thenReturn("password");
    }

    @Test
    public void testUser()
    {
        Mockito.when(distributedConfigProperties.getUserURL()).thenReturn("https://172.16.238.191:8443/fineract-provider/api/v1/users?tenantIdentifier=default");
        ResponseEntity<String> responseEntity=new ResponseEntity<>(
            "\"officeId\": 1,\n" +
                    "    \"resourceId\": 16",
            HttpStatus.OK);
        HttpEntity<AppUser> requestEntity=new HttpEntity<AppUser>(getAppUser(),httpHeaders);
        Mockito.when(restTemplate.exchange(distributedConfigProperties.getUserURL(),HttpMethod.POST,requestEntity, String.class)).thenReturn(responseEntity);
        String result=userController.createUser(getAppUser());
        Assert.assertNotNull(result);
    }

    @Test
    public void testUserUpdate()
    {
        Mockito.when(distributedConfigProperties.getUpdateURL()).thenReturn("https://172.16.238.191:8443/fineract-provider/api/v1/users/{userId}");
        ResponseEntity<String> responseEntity=new ResponseEntity<>("{\"officeId\":1,\"resourceId\":12,\"changes\":{\"firstname\":\"Testing\"}}",
                HttpStatus.OK);
        String request="{\n" +
                "    \"firstname\": \"Testing\"\n" +
                "}";
        HttpEntity<String> requestEntity=new HttpEntity<String>(request,httpHeaders);
        Map<String,Long> params =new HashMap<String,Long>();
        params.put("userId",1L);
        UriComponentsBuilder builder =UriComponentsBuilder.
                fromUriString(distributedConfigProperties.getUpdateURL()).queryParam("tenantIdentifier","default");
        URI uri=builder.buildAndExpand(params).toUri();
        Mockito.when(restTemplate.exchange(uri,HttpMethod.PUT,requestEntity, String.class)).thenReturn(responseEntity);
        String result=userController.userUpdate(1L,request);
        Assert.assertNotNull(result);
    }

    @Test
    public void testUserAuth()
    {
        Mockito.when(distributedConfigProperties.getAuthUrl()).thenReturn("https://172.16.238.191:8443/fineract-provider/api/v1/authentication");
        HttpEntity<String> requestEntity=new HttpEntity<String>(httpHeaders);
        Map<String,String> params =new HashMap<String,String>();
        params.put("username","mifos");
        params.put("password","password");
        UriComponentsBuilder builder =UriComponentsBuilder.
                fromUriString(distributedConfigProperties.getAuthUrl())
                .queryParam("username","mifos")
                .queryParam("password","passsword")
                .queryParam("tenantIdentifier","default");
        URI uri=builder.buildAndExpand(params).toUri();
        ResponseEntity<String> responseEntity=new ResponseEntity<>("{\n" +
                "    \"username\": \"maveric\",\n" +
                "    \"userId\": 10,\n" +
                "    \"base64EncodedAuthenticationKey\": \"bWF2ZXJpYzptYXZlcmlj\",\n" +
                "    \"authenticated\": true,\n" +
                "    \"officeId\": 1,\n" +
                "    \"officeName\": \"Head Office\",\n" +
                "    \"staffId\": 1,\n" +
                "    \"staffDisplayName\": \"A, Aliya\",\n" +
                "    \"roles\": [\n" +
                "        {\n" +
                "            \"id\": 1,\n" +
                "            \"name\": \"Super user\",\n" +
                "            \"description\": \"This role provides all application permissions.\",\n" +
                "            \"disabled\": false\n" +
                "        }\n" +
                "    ],\n" +
                "    \"permissions\": [\n" +
                "        \"ALL_FUNCTIONS\"\n" +
                "    ],\n" +
                "    \"shouldRenewPassword\": false,\n" +
                "    \"isTwoFactorAuthenticationRequired\": false\n" +
                "}",
                HttpStatus.OK);
        Mockito.when(restTemplate.exchange(uri,HttpMethod.POST,requestEntity, String.class)).thenReturn(responseEntity);
        String result=userController.userAuth("mifos","password");
        Assert.assertNotNull(result);
    }


    private AppUser getAppUser()
    {
        List<Integer> list=new ArrayList<>();
        list.add(2);

        return AppUser.builder()
                .email("a@gmail.com")
                .firstname("abc")
                .lastname("def")
                .officeId(2)
                .password("abc")
                .repeatPassword("abc")
                .roles(list)
                .sendPasswordToEmail("false")
                .staffId(2)
                .username("112209")
                .build();
    }
}
